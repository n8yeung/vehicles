import React, { Component } from 'react';
import './App.css';
import VehiclesContainer from './VehiclesContainer/VehiclesContainer';

class App extends Component {

  render() {
    return (
      <div className="App">
        <VehiclesContainer />
      </div>
    );
  }
}

export default App;
